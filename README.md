# Portuguese Sponge Cake Recipe
This recipe is faithful to a mid XX century version, 
passed down by someone that used to bake this cake in what was back then the most popular tea house in Alfeizerão, Portugal.  
While variations of the recipe can be easily and publicly found, 
this one tries to capture as accurately as possible the non-commercial version that she cooked for her family.  
The main differences seem to be the usage of brown sugar and *aguardente velha*, a Portuguese spirit.

## Preparation

## Ingredients
* 9 chicken eggs
* 150 grams of brown sugar
* 75 grams of white flour
* 1 tea spoon of cinamon
* 1 lemon
* 1 shot glass of *Aguardente Velha* (an aged wine based spirit with approximately 40% of alcohol, brandy should work too)

## Tools
* Mixer
* Oven
* 1 big bowl
* 3 small bowls
* 1 cake pan
* Greaseproof paper

## Mise en place
### Eggs
* Separate the yoks from the whites in 6 of the 9 eggs
* Open the remaining 3 eggs to the yoks bowl

### Sugar

### Flour

### Cinamon

### Lemon
* Zest a quarter of a lemon

### Spirit

### Oven
Pre-heat the oven to 190 degrees celsius

### Mixing
* Put the sugar in the eggs bowl and mix it until it gets white (a couple of minutes)
* Add the cinamon, the lemon zest and the spirit
* Mix it for several more minutes as this is what will grant volume to the cake (about 10 minutes)
* Sieve the flour to the mixing bowl and manually stirr until mixed

### Cooking
You should put the dough in the oven immediatly after finishing mixing it, as the volume will decrease overtime.  
This cake should cook in about 15 minutes. If the oven temperature is right, you can remove it when the top has a dark brown hue to it.  
Lower oven temperatures will get you a dry cake and higher ones a gooey one.

### Plating
Do not try to remove the paper. Pick the cake up with the paper and move it to its serving plate.
